(function () {
  'use strict';

  module.exports = ['$httpProvider', function ($httpProvider) {
    delete $httpProvider.defaults.headers.common["If-Modified-Since"];
  }];

}());