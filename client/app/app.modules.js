(function () {
  'use strict';

  angular.module('app', [
      'ngRoute',
      'ngResource',
      'ui.bootstrap',
      'angular-loading-bar',
      '720kb.tooltips',
      'ngAnimate',
      'ui.select',
      'ngSanitize',
      'oitozero.ngSweetAlert',
      'duScroll',
      'ngCookies',
      require('./components').name,
      require('./shared').name
    ])
    .config(require('./app.routes'))
    .config(require('./app.registration'))
    .service('Query', require('./app.queries'));

}());