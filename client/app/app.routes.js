(function () {
  'use strict';

  module.exports = ['$routeProvider', 'Config', function ($routeProvider, Config) {
    $routeProvider
      .when('/cabinet', {
        templateUrl: Config.rootPath + 'components/home/home-view.html',
        controller: 'home'
      })
      .when('/aim', {
        templateUrl: Config.rootPath + 'components/seed-help/seed-help-view.html',
        controller: 'seedHelp'
      })
      .when('/create', {
        templateUrl: Config.rootPath + 'components/create-menu/create-menu-view.html',
        controller: 'createMenu'
      })
      .when('/menus', {
        templateUrl: Config.rootPath + 'components/menus/menus-view.html',
        controller: 'menus'
      })
      .when('/home', {
        templateUrl: Config.rootPath + 'components/intro/intro-view.html',
        controller: 'intro'
      })
      .otherwise({
        redirectTo: '/home'
      });
  }];

}());