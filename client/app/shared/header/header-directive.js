(function () {
  'use strict';

  module.exports = ['Config', function (Config) {

    function link(scope) {
      scope.navLinks = [
        {
          title: 'Личный кабинет',
          href: 'cabinet',
          icon: 'user'
        },
        {
          title: 'Цель',
          href: 'aim',
          icon:'bar-chart'
        },
        {
          title: 'Составить меню',
          href: 'create',
          icon:'cutlery'
        },
        {
          title: 'Мои меню',
          href: 'menus',
          icon:'calendar'
        }
      ];
    }

    return {
      templateUrl: Config.rootPath + 'shared/header/header-view.html',
      link: link,
      replace: true
    };
  }];

}());