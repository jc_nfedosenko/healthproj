(function () {
  'use strict';

  module.exports = ['Config', function (Config) {

    function link(scope, element, attributes) {
      scope.isCollapsed = true;
    }

    return {
      templateUrl: Config.rootPath + 'shared/panel/panel-view.html',
      link: link,
      transclude: true,
      scope: {
        panelName: '=',
        icon: '@'
      }
    };
  }];

}());
