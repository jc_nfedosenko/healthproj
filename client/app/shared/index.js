module.exports = angular.module('shared', [
  require('./header').name,
  require('./navigation').name,
  require('./panel').name
])
  .constant('Config', require('./config'));