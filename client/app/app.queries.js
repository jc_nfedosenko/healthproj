(function () {
  'use strict';

  module.exports = ['$resource', 'Config', function ($resource, Config) {
    return {
      food: $resource(Config.apiPath + 'foods/:id', {
        id: '@id'
      }, {
        get: {
          method: 'GET',
          isArray: true
        },
        getOne: {
          method: 'GET',
          isArray: false
        }
      }),
      menu: $resource(Config.apiPath + 'menus/:id', {
        id: '@id'
      }, {
        create: {
          method: 'POST'
        },
        get: {
          method: 'GET',
          isArray: true
        },
        remove: {
          method: 'DELETE'
        }
      }),
      user: $resource(Config.apiPath + 'users', {}, {
        create: {
          method: 'POST'
        },
        get: {
          method: 'GET',
          isArray: true
        }
      }),
      addToMenu: $resource(Config.apiPath + 'users/addfoods', {}, {
        add: {
          method: 'POST',
          isArray: true
        }
      }),
      getFoodMenu: $resource(Config.apiPath + 'showmenufood?name=:menuName&user_id=:userId', {
        menuName: '@menuName',
        userId: '@userId'
      }, {
        get: {
          method: 'GET',
          isArray: true
        }
      })
    };
  }];

}());
