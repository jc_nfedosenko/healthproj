(function () {
  'use strict';

  module.exports = ['$scope', 'Query', '$cookies', '$cookieStore', function ($scope, Query, $cookies, $cookieStore) {
    $scope.panelNames = {
      anketPanel: 'анкетные данные',
      parametersPanel: 'параметры'
    };
    $scope.getNumber = function (num) {
      return new Array(num);
    };
    $scope.person = $cookieStore.get('user');
    console.log($scope.person);
  }];

}());