(function () {
  'use strict';

  module.exports = ['$scope', '$timeout', 'Query', '$cookieStore', function ($scope, $timeout, Query, $cookieStore) {
    Query.food.get(function (data) {
      $scope.products = data;
      $scope.products.forEach(function (elem) {
        elem.quantity = 1;
      });
    });
    $scope.person= $cookieStore.get('user');
    $scope.maxCalories = 2000;
    $scope.menu = {
      name: '',
      productsArray: [],
      totalCalories: 0
    };
    $scope.getTotalCalories = function () {
      var total = 0;
      if ($scope.menu.productsArray) {
        $scope.menu.productsArray.forEach(function (elem) {
          total += elem.calories * elem.quantity;
        });
      }
      console.log(total);
      if (total < $scope.maxCalories) {
        $scope.menu.totalCalories = total;
      } else {
        swal({
          title: "Слишком много калорий",
          text: "Выберите другие продукты, либо измените количество!",
          type: "error",
          confirmButtonColor: "#45c89f",
          confirmButtonText: "Хорошо!",
          closeOnConfirm: true
        });
      }
    };
    $scope.removeProduct = function (product) {
      swal({
          title: "Вы уверены?",
          text: "Продукт будет удален из вашего меню!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#45c89f",
          confirmButtonText: "Да,удалить!",
          closeOnConfirm: true
        },
        function () {
          swal('close');
          $timeout(function () {
            var startArr = $scope.menu.productsArray,
              neededIndex = 0;
            startArr.forEach(function (elem, index) {
              if (elem.name === product.name) {
                neededIndex = index;
              }
            });
            startArr.splice(neededIndex, 1);
            $scope.getTotalCalories();
          }, 50);
        });
    };
    $scope.submitMenu = function () {
      if ($scope.menu.name === '') {
        swal({
          title: "Ошибка",
          text: "Введите название меню!",
          type: "error",
          confirmButtonColor: "#45c89f",
          closeOnConfirm: true
        });
      } else {
        var menuToPost = {
            name: $scope.menu.name,
            id: $scope.person.id
          },
          productsToPost = {
            menus: []
          };
        $scope.menu.productsArray.forEach(function (elem) {
          var product = {
            user_id: $scope.person.id,
            food_id: elem.id,
            quantity: elem.quantity,
            name: $scope.menu.name
          }
          productsToPost.menus.push(product);
        });
        Query.menu.create(angular.toJson(menuToPost));
        Query.addToMenu.add(angular.toJson(productsToPost));
        console.log(angular.toJson(productsToPost));
        $scope.menu = {};
        $scope.products.forEach(function (elem) {
          elem.quantity = 1;
        });
        swal({
          title: "Меню удачно сохранено!",
          type: "success",
          confirmButtonColor: "#45c89f",
          closeOnConfirm: true
        });
      }
    };
    $scope.$watch('menu.productsArray', function () {
      $scope.getTotalCalories();
    });
  }];
}());