module.exports = angular.module('components', [
  require('./home').name,
  require('./seed-help').name,
  require('./menus').name,
  require('./create-menu').name,
  require('./intro').name
]);