(function () {
  'use strict';

  module.exports = ['$scope', 'Query', '$timeout', '$cookieStore', function ($scope, Query, $timeout, $cookieStore) {
    Query.menu.get({
      id: 2
    }, function (data) {
      $scope.person = $cookieStore.get('user');
      console.log($scope.person);
      $scope.menus = data;
      $scope.menus.forEach(function (menu) {
        //console.log(menu);
        Query.getFoodMenu.get({
          menuName: menu.name,
          userId: $scope.person.id
        }, function (foods) {
          var dishes = [];
          foods.forEach(function (dish) {
            //console.log(dish);
            Query.food.getOne({
              id: dish.food_id
            }, function (callback) {
              callback.quantity = dish.quantity;
              dishes.push(callback);
            });
          });
          menu.dishes = dishes;
        });
      });

    });
    $scope.deleteMenu = function (menu) {
      swal({
          title: "Вы уверены?",
          text: "Продукт будет удален из вашего меню!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#45c89f",
          confirmButtonText: "Да,удалить!",
          closeOnConfirm: true
        },
        function () {
          swal('close');
          Query.menu.remove({
            id: menu.id
          }, function () {
            console.log('Deleted');
          });
          $timeout(function () {
            var startArr = $scope.menus,
              neededIndex = 0;
            startArr.forEach(function (elem, index) {
              if (elem.id === menu.id) {
                neededIndex = index;
              }
            });
            $scope.menus.splice(neededIndex, 1);
          }, 50);
        });
    };
  }];

}());