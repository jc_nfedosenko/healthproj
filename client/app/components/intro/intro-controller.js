(function () {
  'use strict';

  module.exports = ['$scope', 'Query', '$location', '$cookies', '$cookieStore', function ($scope, Query, $location, $cookies, $cookieStore) {
    $scope.person = {
      id: 2
    };
    $scope.getNumber = function (num) {
      return new Array(num);
    };
    $scope.register = function () {
      console.log($scope.person);
      $cookieStore.put('user', $scope.person);
      $scope.person = {};
      swal({
        title: "Вы удачно зарегистрировались!",
        type: "success",
        confirmButtonColor: "#45c89f",
        closeOnConfirm: true
      }, function () {

      });
      $location.path('/cabinet');
    };
  }];

}());