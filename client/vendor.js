(function () {
  'use strict';

  require('angular');
  require('angular-route');
  require('angular-resource');
  require('angular-bootstrap');
  require('angular-animate');
  require('ui-select');
  require('ui-select');
  require('angular-sanitize');
  require('ngSweetAlert');
  require('sweetalert');
  require('angular-loading-bar');
  require('angular-tooltips');
  require('angular-scroll');
  require('angular-cookies');
}());